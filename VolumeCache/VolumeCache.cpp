#include "VolumeCache.h"

#include <boost/thread/locks.hpp>
#include <Field3D/InitIO.h>

#include <openvdb/math/Vec3.h>

CacheInstance::CacheInstance() 
: _valid(false)
{}

CacheInstance::~CacheInstance(){}

Field3DCacheInstance::Field3DCacheInstance(Field3D::SparseField<float>::Ptr field, Field3D::MatrixFieldMapping::Ptr mapping)
: _field(field), _mapping(mapping)
{
    _valid = true;
    _cacheType = kField3D;
}

float Field3DCacheInstance::lookup(const Imath::V3d& wsP) const 
{
    Imath::V3d vsP;
    _mapping->worldToVoxel(wsP, vsP);
    return _lint.sample(*_field, vsP);
}

void Field3DCacheInstance::bounds(Imath::V3d& bbmin, Imath::V3d& bbmax) const 
{
    _mapping->localToWorld().multVecMatrix(Imath::V3d(0,0,0), bbmin);
    _mapping->localToWorld().multVecMatrix(Imath::V3d(1,1,1), bbmax);
}

size_t Field3DCacheInstance::memUsage() const 
{
    return _field->memSize();
}

Imath::V3d Field3DCacheInstance::voxelSize() const
{
    Imath::V3d a, b;
    _mapping->voxelToWorld().multVecMatrix(Imath::V3d(0,0,0), a);
    _mapping->voxelToWorld().multVecMatrix(Imath::V3d(1,1,1), b);
    return b - a;
}

//OpenVDBCacheInstance::FloatGridSamplerPtr OpenVDBCacheInstance::_gridSampler;

OpenVDBCacheInstance::OpenVDBCacheInstance(VolumeCache::Handle h, openvdb::FloatGrid::Ptr grid)
: _grid(grid),_handle(h)
{
    _gridSampler = FloatGridSampler::Ptr(new FloatGridSampler(_grid->tree(), _grid->constTransform()));
    _valid = true;
    _cacheType = kOpenVDB;
}

float OpenVDBCacheInstance::lookup(const Imath::V3d& wsP) const 
{
    return _gridSampler->wsSample(&(wsP.x));
}

void OpenVDBCacheInstance::bounds(Imath::V3d& bbmin, Imath::V3d& bbmax) const 
{
    openvdb::CoordBBox bbox = _grid->evalActiveVoxelBoundingBox();
    openvdb::Vec3d mn = bbox.min().asVec3d();
    openvdb::Vec3d mx = bbox.max().asVec3d();
    mn = _grid->indexToWorld(mn);
    mx = _grid->indexToWorld(mx);
    bbmin = Imath::V3d(mn.x(), mn.y(), mn.z());
    bbmax = Imath::V3d(mx.x(), mx.y(), mx.z());
}

size_t OpenVDBCacheInstance::memUsage() const 
{
    return _grid->memUsage();
}

Imath::V3d OpenVDBCacheInstance::voxelSize() const
{
    openvdb::Vec3d v = _grid->voxelSize();
    return Imath::V3d(v.x(), v.y(), v.z());
}

VolumeCache::StaticInit VolumeCache::_staticInit;
VolumeCache* VolumeCache::_instance(0);
VolumeCache::StaticInit::StaticInit()
{
    Field3D::initIO();
    openvdb::initialize();
}

VolumeCache* VolumeCache::instance()
{
    if (!_instance)
    {
        boost::mutex::scoped_lock m;
        if (!_instance)
        {
            _instance = new VolumeCache;
        }
    }
    return _instance;
}

CacheInstance::Ptr VolumeCache::getCacheInstance(Handle h)
{
    CacheInstance::Ptr c;
    if (h >= 0)
    {
        CacheInstanceMap::iterator it = _cacheInstanceMap.find(h);
        if (it != _cacheInstanceMap.end())
        {
            c = it->second;
        }
    }

    return c;
}

VolumeCache::Handle VolumeCache::readFloatField(const std::string& cacheName, const std::string& fieldName)
{
    Handle h;

    boost::mutex::scoped_lock lock;

    // check in our cache entries to see if we've loaded this cache already
    std::string fileHandle = cacheName + std::string("/") + fieldName;
    FileHandleMap::const_iterator it = _fileHandleMap.find(fileHandle);
    if (it != _fileHandleMap.end())
    {
        // just return the existing handle
        h = it->second;
    }
    else
    {
        // try and load it
        // first figure out what type of file we have
        std::string ext = cacheName.substr(cacheName.length()-3, std::string::npos);
        if (ext == "f3d")
        {
            h = readField3DField(cacheName, fieldName);
            _fileHandleMap[fileHandle] = h;
        }
        else if (ext == "vdb")
        {
            h = readOpenVDBGrid(cacheName, fieldName);
            _fileHandleMap[fileHandle] = h;
        }
        else
        {
            std::cerr << "[VolumeCache] Unsupported file type with extension '" << ext << "'" << std::endl;
        }
    }

    return h;
}

void VolumeCache::evict(Handle h)
{
    CacheInstanceMap::iterator it = _cacheInstanceMap.find(h);
    if (it != _cacheInstanceMap.end()) _cacheInstanceMap.erase(it);
}

size_t VolumeCache::memUsage() const
{
    CacheInstanceMap::const_iterator it = _cacheInstanceMap.begin();
    size_t mem = 0;
    for (; it != _cacheInstanceMap.end(); ++it)
    {
        mem += it->second->memUsage();
    }
    return mem;
}

VolumeCache::Handle VolumeCache::readField3DField(const std::string& cacheName, const std::string& fieldName)
{
    Handle h;
    Field3D::Field3DInputFile in;
    if (in.open(cacheName.c_str()))
    {
        // split the fieldName into partition and layer, if the '.' seperator is used
        Field3D::SparseField<float>::Vec fvec;
        size_t pos = fieldName.find(".");
        if (pos == std::string::npos)
        {
            fvec = in.readScalarLayersAs<Field3D::SparseField, float>(fieldName);
        }
        else
        {
            std::string partition = fieldName.substr(0, pos);
            std::string layer = fieldName.substr(pos+1, std::string::npos);
            fvec = in.readScalarLayersAs<Field3D::SparseField, float>(partition, layer);
        }

        if (fvec.size())
        {
            Field3D::SparseField<float>::Ptr inField = fvec[0];         
            if (inField)
            {
                Field3D::MatrixFieldMapping::Ptr inMapping 
                    = boost::dynamic_pointer_cast<Field3D::MatrixFieldMapping>(inField->mapping());
                if (inMapping)
                {
                    CacheInstance::Ptr c( new Field3DCacheInstance(inField, inMapping));
                    h = _numEntries++;
                    _cacheInstanceMap[h] = c;
                }
                else
                {
                    std::cerr << "[VolumeCache] could not get mapping from field '" << fieldName << "' in file '" << cacheName << "'" << std::endl;
                    h = kInvalidField;
                }
            }
            else
            {
                std::cerr << "[VolumeCache] SparseField '" << fieldName << "' was NULL in '" << cacheName << "'" << std::endl;
                h = kInvalidField;
            }
        }
        else
        {
            std::cerr << "[VolumeCache] could not find SparseField '" << fieldName << "' in file '" << cacheName << "'" << std::endl;
            h = kInvalidField;
        }
    }
    else
    {
        std::cerr << "[VolumeCache] could not open file '" << cacheName << "'" << std::endl;
        h = kInvalidFile;
    }
    
    return h;
}

VolumeCache::Handle VolumeCache::readOpenVDBGrid(const std::string& cacheName, const std::string& gridName)
{
    VolumeCache::Handle h;
    try
    {
        openvdb::io::File file(cacheName);
        file.open();
        openvdb::GridBase::Ptr bg = file.readGrid(gridName);
        file.close();
        FloatGridType::Ptr grid = openvdb::gridPtrCast<openvdb::FloatGrid>(bg);
        if (grid)
        {
            h = _numEntries++;
            OpenVDBCacheInstance::Ptr oc(new OpenVDBCacheInstance(h, grid));
            _cacheInstanceMap[h] = oc;
        }
        else
        {
            h = -1;
        }
    }
    catch (openvdb::Exception& e)
    {
        std::cerr << "[VolumeCache] " << e.what() << std::endl;
        h = kInvalidFile;
    }

    return h;
}
