import pymel.core as pm
from alShaders import alShadersTemplate

class AEalVolumeTemplate(alShadersTemplate):
    def setup(self):
        self.addSwatch()
        self.beginScrollLayout()

        self.addControl('filename')
        self.addControl('densityMult')
        self.addControl('densityClip')
        self.addControl('scattering')
        self.addControl('g')
        self.addControl('absorption')

        pm.mel.AEdependNodeTemplate(self.nodeName)

        self.addExtraControls()
        self.endScrollLayout()